function createCard(name, description, pictureUrl, startDate, endDate, locationName) {
    return `
        <div class="card">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-body-secondary">${locationName}</h6>
            <p class="card-text">${description}</p>
            <div class="card-footer text-body-secondary">${startDate}-${endDate}</div>
          </div>
      </div>
    </div>
    `;
  }

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);
      console.log("response is bad");

      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();

        for (let conference of data.conferences){
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                const name = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const startDate = Date(details.conference.starts);
                const endDate = details.conference.ends;
                const locationName = details.conference.location.name;
                const html = createCard(name, description, pictureUrl, startDate, endDate, locationName);
                const column = document.querySelector(".col");
                column.innerHTML += html;
            }
        }
    }
} catch (e) {
// Figure out what to do if an error is raised
    console.error(e)
}

});
